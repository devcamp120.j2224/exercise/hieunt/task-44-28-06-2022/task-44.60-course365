/*** VÙNG 1: BIẾN TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
/*** VÙNG 2: GÁN SỰ KIỆN CHO CÁC PHẦN TỬ */
$(document).ready(function() {
    var vPopularCourseList = getPopularCourses();
    displayPopularCourses(vPopularCourseList);
    var vTrendingCourseList = getTrendingCourses();
    displayTrendingCourses(vTrendingCourseList)
})

/*** VÙNG 3: HÀM XỬ LÝ SỰ KIỆN */

/*** VÙNG 4: HÀM DÙNG CHUNG */
function getPopularCourses() {
    var vPopularCourseList = gCoursesDB.courses.filter(function(item) {
        return (item.isPopular == true)
    })
    return vPopularCourseList
}
function getTrendingCourses() {
    var vTrendingCourseList = gCoursesDB.courses.filter(function(item) {
        return (item.isTrending == true)
    })
    return vTrendingCourseList
}
function displayPopularCourses(paramCourseList) {
    console.log("Danh sách course Popular");
    console.log(paramCourseList);
    var vPopularCarDeck = $("div.popular").children("div:nth-child(2)").children("div");
    for (let bCourse of paramCourseList) {
        vPopularCarDeck
        .append($("<div>").addClass("card")
            .append($("<img>").addClass("card-img-top").attr("src", bCourse.coverImage))
            .append($("<div>").addClass("card-body")
                .append($("<h6>").addClass("card-title courseName")
                    .append($("<a>").attr("href", "#").html(bCourse.courseName))
                )
                .append($("<i>").addClass("far fa-clock"))
                .append($("<span>").addClass("duration").html(" " + bCourse.duration))
                .append($("<span>").addClass("level").html(" " + bCourse.level))
                .append($("<p>").addClass("card-text pt-2")
                    .html("$<span class='discountPrice'><b>" + bCourse.discountPrice + "</b></span>"
                        + " $<span class='price text-success'><del>" + bCourse.price + "</del></span>"
                    )
                )
            )
            .append($("<div>").addClass("card-footer")
                .append($("<img>").addClass("teacherPhoto rounded-circle img-thumbnail")
                    .attr({
                        "src": bCourse.teacherPhoto,
                        "width": "50px",
                        "height": "50px"
                    })
                )
                .append($("<span>").addClass("teacherName pl-3").html(bCourse.teacherName))
                .append($("<i>").addClass("far fa-bookmark float-right"))
            )
        )
    }
}
function displayTrendingCourses(paramCourseList) {
    console.log("Danh sách course Trending");
    console.log(paramCourseList);
    var vTrendingCarDeck = $("div.trending").children("div:nth-child(2)").children("div");
    for (let bCourse of paramCourseList) {
        vTrendingCarDeck
        .append($("<div>").addClass("card")
            .append($("<img>").addClass("card-img-top").attr("src", bCourse.coverImage))
            .append($("<div>").addClass("card-body")
                .append($("<h6>").addClass("card-title courseName")
                    .append($("<a>").attr("href", "#").html(bCourse.courseName))
                )
                .append($("<i>").addClass("far fa-clock"))
                .append($("<span>").addClass("duration").html(" " + bCourse.duration))
                .append($("<span>").addClass("level").html(" " + bCourse.level))
                .append($("<p>").addClass("card-text pt-2")
                    .html("$<span class='discountPrice'><b>" + bCourse.discountPrice + "</b></span>"
                        + " $<span class='price text-success'><del>" + bCourse.price + "</del></span>"
                    )
                )
            )
            .append($("<div>").addClass("card-footer")
                .append($("<img>").addClass("teacherPhoto rounded-circle img-thumbnail")
                    .attr({
                        "src": bCourse.teacherPhoto,
                        "width": "50px",
                        "height": "50px"
                    })
                )
                .append($("<span>").addClass("teacherName pl-3").html(bCourse.teacherName))
                .append($("<i>").addClass("far fa-bookmark float-right"))
            )
        )
    }
}