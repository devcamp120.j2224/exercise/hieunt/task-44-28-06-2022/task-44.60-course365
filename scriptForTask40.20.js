$(document).ready(function() {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-css.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    }
    var gColNameList = [
        "id", "courseCode", "courseName", "price", "discountPrice", "duration", "level",
        "coverImage", "teacherName", "teacherPhoto", "isPopular", "isTrending", "action",
    ];
    const gCOL_ID = 0;
    const gCOL_COURSE_CODE = 1;
    const gCOL_COURSE_NAME = 2;
    const gCOL_PRICE = 3;
    const gCOL_DISCOUNT_PRICE = 4;
    const gCOL_DURATION = 5;
    const gCOL_LEVEL = 6;
    const gCOL_COVER_IMAGE = 7;
    const gCOL_TEACHER_NAME = 8;
    const gCOL_TEACHER_PHOTO = 9;
    const gCOL_IS_POPULAR = 10;
    const gCOL_IS_TRENDING = 11;
    const gCOL_ACTION = 12;
    var gStt = 0;
    var gCourseTable = $("#course-table").DataTable({
        columns: [
            {data: gColNameList[gCOL_ID]},
            {data: gColNameList[gCOL_COURSE_CODE]},
            {data: gColNameList[gCOL_COURSE_NAME]},
            {data: gColNameList[gCOL_PRICE]},
            {data: gColNameList[gCOL_DISCOUNT_PRICE]},
            {data: gColNameList[gCOL_DURATION]},
            {data: gColNameList[gCOL_LEVEL]},
            {data: gColNameList[gCOL_COVER_IMAGE]},
            {data: gColNameList[gCOL_TEACHER_NAME]},
            {data: gColNameList[gCOL_TEACHER_PHOTO]},
            {data: gColNameList[gCOL_IS_POPULAR]},
            {data: gColNameList[gCOL_IS_TRENDING]},
            {data: gColNameList[gCOL_ACTION]},
        ],
        columnDefs: [
            {
                targets: gCOL_ID,
                render: function() {
                    gStt ++;
                    return gStt
                }
            },
            {
                targets: gCOL_COVER_IMAGE,
                render: function(data) {
                    var vCoverImage = "<img src='" + data + "' width='100px'></img>";
                    return vCoverImage
                }
            },
            {
                targets: gCOL_TEACHER_PHOTO,
                render: function(data) {
                    var vTeacherPhoto = "<img src='" + data + "' width='50px' height='50px'></img>";
                    return vTeacherPhoto
                },
            },
            {
                targets: gCOL_ACTION,
                defaultContent: `
                    <div class='btn-group'>
                        <button class='btn btn-primary btn-edit'>Sửa</button>
                        <button class='btn btn-danger btn-delete'>Xóa</button>
                    </div>
                `
            }
        ],
    });
    var gCourseId = 0;
    var gCourseCode = "";

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    $("#btn-add-course").on("click", onBtnAddCourseClick)
    $("#course-table").on("click", ".btn-edit", function(){
        onBtnEditClick(this)
    });
    $("#course-table").on("click", ".btn-delete", function(){
        onBtnDeleteClick(this)
    });
    $("#btn-confirm-add-course").on("click", onBtnConfirmAddCourseClick)
    $("#btn-update-course").on("click", onBtnUpdateCourseClick)
    $("#btn-confirm-delete-course").on("click", onBtnConfirmDeleteCourseClick)

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        loadDataToTable(gCoursesDB.courses)
    }
    function onBtnAddCourseClick() {
        $("#modal-add-course").modal("show");
    }
    function onBtnEditClick(paramButtonElement) {
        "use strict";
        var vCurrentRow = $(paramButtonElement).closest("tr");
        var vRowData = gCourseTable.row(vCurrentRow).data();
        gCourseId = vRowData.id; // lưu Id của course vào 1 biến toàn cục
        gCourseCode = vRowData.courseCode; // lưu mã của course vào 1 biến toàn cục
        console.log("Nút Sửa được ấn. Id là: " + gCourseId);
        loadDataToModalUpdateUser(vRowData);
        $("#modal-update-course").modal("show");
    }
    function onBtnDeleteClick(paramButtonElement) {
        "use strict";
        var vCurrentRow = $(paramButtonElement).closest("tr");
        var vRowData = gCourseTable.row(vCurrentRow).data();
        gCourseId = vRowData.id;
        console.log("Nút Xóa được ấn. Id là: " + gCourseId);
        $("#modal-delete-course").modal("show");
    }
    function onBtnConfirmAddCourseClick() {
        var vCourseObjToAdd = {
            "id": 0,
            "courseCode": "", 
            "courseName": "", 
            "price": 0, 
            "discountPrice": 0, 
            "duration": "", 
            "level": "",
            "coverImage": "", 
            "teacherName": "", 
            "teacherPhoto": "", 
            "isPopular": false, 
            "isTrending": false,
        };
        //bước 1: lấy dữ liệu từ các input
        getDataFromModalAddCourse(vCourseObjToAdd);
        console.log("Ghi tạm đối tượng dữ liệu ra console");
        console.log(vCourseObjToAdd);
        //bước 2: kiểm tra dữ liệu lấy từ các input
        var vCheckResult = validateCourseDataToAdd(vCourseObjToAdd);
        console.log("Kết quả kiểm tra dữ liệu là: " + vCheckResult);
        if (vCheckResult == true) {
        //bước 3: tạo course mới
            gCoursesDB.courses.push(vCourseObjToAdd);
            alert("Tạo course mới thành công! Id course mới là " + vCourseObjToAdd.id);
            loadDataToTable(gCoursesDB.courses);
            resetModalAddCourse();
            $("#modal-add-course").modal("hide")
        }
    }
    function onBtnUpdateCourseClick() {
        var vCourseObjToUpdate = {
            "id": gCourseId,
            "courseCode": "", 
            "courseName": "", 
            "price": 0, 
            "discountPrice": 0, 
            "duration": "", 
            "level": "",
            "coverImage": "", 
            "teacherName": "", 
            "teacherPhoto": "", 
            "isPopular": false, 
            "isTrending": false,
        };
        //bước 1: lấy dữ liệu từ các input
        getDataFromModalUpdateUser(vCourseObjToUpdate);
        console.log("Ghi tạm đối tượng dữ liệu ra console");
        console.log(vCourseObjToUpdate);
        //bước 2: kiểm tra dữ liệu lấy từ các input
        var vCheckResult = validateCourseDataToUpdate(vCourseObjToUpdate);
        console.log("Kết quả kiểm tra dữ liệu là: " + vCheckResult);
        if (vCheckResult == true) {
        //bước 3:  update course data
        updateCourseData(vCourseObjToUpdate);
        loadDataToTable(gCoursesDB.courses);
        resetModalUpdateCourse();
        $("#modal-update-course").modal("hide")
        }
    }
    function onBtnConfirmDeleteCourseClick() {
        removeCourseFromCourseList(gCourseId);
        loadDataToTable(gCoursesDB.courses);
        $("#modal-delete-course").modal("hide")
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function loadDataToTable(paramCourseList) {
        gStt = 0;
        gCourseTable.clear()
            .rows.add(paramCourseList)
            .draw();
    }
    function getDataFromModalAddCourse(paramCourseObject) { //hàm đọc dữ liệu từ các input
        paramCourseObject.id = createNewId();
        paramCourseObject.courseCode = $.trim($("#inp-add-course-code").val());
        paramCourseObject.courseName = $.trim($("#inp-add-course-name").val());
        paramCourseObject.price = Number($("#inp-add-course-price").val());
        paramCourseObject.discountPrice = Number($("#inp-add-course-discount-price").val());
        if (paramCourseObject.discountPrice == 0) {
            paramCourseObject.discountPrice = paramCourseObject.price
        };
        paramCourseObject.duration = $.trim($("#inp-add-course-duration").val()); 
        paramCourseObject.level = $.trim($("#inp-add-course-level").val());
        paramCourseObject.coverImage = $.trim($("#inp-add-course-image").val()); 
        paramCourseObject.teacherName = $.trim($("#inp-add-course-teacher-name").val()); 
        paramCourseObject.teacherPhoto = $.trim($("#inp-add-course-teacher-image").val()); 
        paramCourseObject.isPopular = $("#chkb-add-course-is-popular").is(":checked"); 
        paramCourseObject.isTrending = $("#chkb-add-course-is-trending").is(":checked");
    }
    function createNewId() {
        var vNewId = 0;
        if (gCoursesDB.courses.length == 0) {
            vNewId = 1
        }
        else {
            vNewId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1
        }
        return vNewId
    }
    function validateCourseDataToAdd(paramCourseObject) { //hàm kiểm tra dữ liệu lấy từ các input
        var vCheckResult = false;
        if (paramCourseObject.courseCode == "") {
            console.log("Chưa nhập Mã Course");
            alert("Chưa nhập Mã Course")
        }
        else if (checkIfCourseCodeToAddIsExisted(paramCourseObject.courseCode) == true) {
            console.log("Đã tồn tại Mã Course này");
            alert("Đã tồn tại Mã Course này")
        }
        else if (paramCourseObject.courseName == "") {
            console.log("Chưa nhập Course name");
            alert("Chưa nhập Course name")
        }
        else if (paramCourseObject.price == "") {
            console.log("Chưa nhập Giá gốc");
            alert("Chưa nhập Giá gốc")
        } 
        else if (Number.isInteger(paramCourseObject.price) == false || paramCourseObject.price <= 0) {
            console.log("Giá Gốc phải là số nguyên > 0");
            alert("Giá Gốc phải là số nguyên > 0")
        }
        else if (paramCourseObject.discountPrice > paramCourseObject.price) {
            console.log("Giá giảm phải nhỏ hơn hoặc bằng Giá gốc");
            alert("Giá giảm phải nhỏ hơn hoặc bằng Giá gốc")
        }
        else if (Number.isInteger(paramCourseObject.discountPrice) == false || paramCourseObject.discountPrice <= 0) {
            console.log("Giá Giảm phải là số nguyên > 0");
            alert("Giá Giảm phải là số nguyên > 0")
        }
        else if (paramCourseObject.duration == "") {
            console.log("Chưa nhập Thời gian");
            alert("Chưa nhập Thời gian")
        }
        else if (paramCourseObject.level == "") {
            console.log("Chưa nhập Cấp độ");
            alert("Chưa nhập Cấp độ")
        }
        else if (paramCourseObject.coverImage == "") {
            console.log("Chưa nhập Link ảnh Khóa học");
            alert("Chưa nhập Link ảnh Khóa học")
        }
        else if (paramCourseObject.teacherName == "") {
            console.log("Chưa nhập Tên Giảng viên");
            alert("Chưa nhập Tên Giảng viên")
        }
        else if (paramCourseObject.teacherPhoto == "") {
            console.log("Chưa nhập Link ảnh Giảng viên");
            alert("Chưa nhập Link ảnh Giảng viên")
        }
        else {
            vCheckResult = true
        }
        return vCheckResult
    }
    function checkIfCourseCodeToAddIsExisted(paramCourseCode) {
        var vIndex = 0;
        //debugger;
        while (vIndex < gCoursesDB.courses.length) {
            if (gCoursesDB.courses[vIndex].courseCode == paramCourseCode) {
                return true
            }
            else {
                vIndex ++
            }
        }
        return false
    }
    function resetModalAddCourse() {
        $("#inp-add-course-code").val("");
        $("#inp-add-course-name").val("");
        $("#inp-add-course-price").val("");
        $("#inp-add-course-discount-price").val("");
        $("#inp-add-course-duration").val(""); 
        $("#inp-add-course-level").val("");
        $("#inp-add-course-image").val(""); 
        $("#inp-add-course-teacher-name").val(""); 
        $("#inp-add-course-teacher-image").val(""); 
        $("#chkb-add-course-is-popular").prop("checked", false); 
        $("#chkb-add-course-is-trending").prop("checked", false);
    }
    function resetModalUpdateCourse() {
        $("#inp-update-course-code").val("");
        $("#inp-update-course-name").val("");
        $("#inp-update-course-price").val("");
        $("#inp-update-course-discount-price").val("");
        $("#inp-update-course-duration").val(""); 
        $("#inp-update-course-level").val("");
        $("#inp-update-course-image").val(""); 
        $("#inp-update-course-teacher-name").val(""); 
        $("#inp-update-course-teacher-image").val(""); 
        $("#chkb-update-course-is-popular").prop("checked", false); 
        $("#chkb-update-course-is-trending").prop("checked", false);
    }
    function loadDataToModalUpdateUser(paramRowData) {
        $("#inp-update-course-code").val(paramRowData.courseCode);
        $("#inp-update-course-name").val(paramRowData.courseName);
        $("#inp-update-course-price").val(paramRowData.price);
        $("#inp-update-course-discount-price").val(paramRowData.discountPrice);
        $("#inp-update-course-duration").val(paramRowData.duration);
        $("#inp-update-course-level").val(paramRowData.level);
        $("#inp-update-course-image").val(paramRowData.coverImage);
        $("#inp-update-course-teacher-name").val(paramRowData.teacherName);
        $("#inp-update-course-teacher-image").val(paramRowData.teacherPhoto);
        $("#chkb-update-course-is-popular").prop("checked", paramRowData.isPopular); 
        $("#chkb-update-course-is-trending").prop("checked", paramRowData.isTrending);
    }
    function getDataFromModalUpdateUser(paramCourseObject) { //hàm đọc dữ liệu từ các input
        paramCourseObject.courseCode = $.trim($("#inp-update-course-code").val());
        paramCourseObject.courseName = $.trim($("#inp-update-course-name").val());
        paramCourseObject.price = Number($("#inp-update-course-price").val());
        paramCourseObject.discountPrice = Number($("#inp-update-course-discount-price").val());
        if (paramCourseObject.discountPrice == 0) {
            paramCourseObject.discountPrice = paramCourseObject.price
        };
        paramCourseObject.duration = $.trim($("#inp-update-course-duration").val()); 
        paramCourseObject.level = $.trim($("#inp-update-course-level").val());
        paramCourseObject.coverImage = $.trim($("#inp-update-course-image").val()); 
        paramCourseObject.teacherName = $.trim($("#inp-update-course-teacher-name").val()); 
        paramCourseObject.teacherPhoto = $.trim($("#inp-update-course-teacher-image").val()); 
        paramCourseObject.isPopular = $("#chkb-update-course-is-popular").is(":checked"); 
        paramCourseObject.isTrending = $("#chkb-update-course-is-trending").is(":checked");
    }
    function validateCourseDataToUpdate(paramCourseObject) { //hàm kiểm tra dữ liệu lấy từ các input
        var vCheckResult = false;
        if (paramCourseObject.courseCode == "") {
            console.log("Chưa nhập Mã Course");
            alert("Chưa nhập Mã Course")
        }
        else if (checkIfCourseCodeToUpdateIsExisted(paramCourseObject.courseCode) == true) {
            console.log("Đã tồn tại Mã Course này");
            alert("Đã tồn tại Mã Course này")
        }
        else if (paramCourseObject.courseName == "") {
            console.log("Chưa nhập Course name");
            alert("Chưa nhập Course name")
        }
        else if (paramCourseObject.price == "") {
            console.log("Chưa nhập Giá gốc");
            alert("Chưa nhập Giá gốc")
        } 
        else if (Number.isInteger(paramCourseObject.price) == false || paramCourseObject.price <= 0) {
            console.log("Giá Gốc phải là số nguyên > 0");
            alert("Giá Gốc phải là số nguyên > 0")
        }
        else if (paramCourseObject.discountPrice > paramCourseObject.price) {
            console.log("Giá giảm phải nhỏ hơn hoặc bằng Giá gốc");
            alert("Giá giảm phải nhỏ hơn hoặc bằng Giá gốc")
        }
        else if (Number.isInteger(paramCourseObject.discountPrice) == false || paramCourseObject.discountPrice <= 0) {
            console.log("Giá Giảm phải là số nguyên > 0");
            alert("Giá Giảm phải là số nguyên > 0")
        }
        else if (paramCourseObject.duration == "") {
            console.log("Chưa nhập Thời gian");
            alert("Chưa nhập Thời gian")
        }
        else if (paramCourseObject.level == "") {
            console.log("Chưa nhập Cấp độ");
            alert("Chưa nhập Cấp độ")
        }
        else if (paramCourseObject.coverImage == "") {
            console.log("Chưa nhập Link ảnh Khóa học");
            alert("Chưa nhập Link ảnh Khóa học")
        }
        else if (paramCourseObject.teacherName == "") {
            console.log("Chưa nhập Tên Giảng viên");
            alert("Chưa nhập Tên Giảng viên")
        }
        else if (paramCourseObject.teacherPhoto == "") {
            console.log("Chưa nhập Link ảnh Giảng viên");
            alert("Chưa nhập Link ảnh Giảng viên")
        }
        else {
            vCheckResult = true
        }
        return vCheckResult
    }
    function checkIfCourseCodeToUpdateIsExisted(paramCourseCode) {
        if (paramCourseCode == gCourseCode) {
            return false
        }
        else {
            var vIndex = 0;
            //debugger;
            while (vIndex < gCoursesDB.courses.length) {
                if (gCoursesDB.courses[vIndex].courseCode == paramCourseCode) {
                    return true
                }
                else {
                    vIndex ++
                }
            }
            return false
        }
    }
    function updateCourseData(paramCourseObject) {
        var vIndex = 0;
        while (vIndex < gCoursesDB.courses.length) { // nếu chưa chạy hết course list
            if (gCoursesDB.courses[vIndex].id == paramCourseObject.id) { // nếu id của course ở index hiện tại bằng Id của course cần update
                gCoursesDB.courses.splice(vIndex, 1, paramCourseObject); // xóa course cũ ở index là vIndex và thêm course mới là paramCourseObject
                alert("Cập nhật thành công cho course có Id là " + paramCourseObject.id + "!");
                return //ngắt vòng lặp
            }
            else {
                vIndex ++ // nếu course có index hiện tại không đúng id thì tăng index thêm 1
            }
        }
        if (vIndex = gCoursesDB.courses.length) { // nếu index chạy qua course cuối cùng vẫn không đúng id
            alert("Cập nhật thất bại!")
        }
    }
    function removeCourseFromCourseList(paramCourseId) {
        var vIndex = 0;
        while (vIndex < gCoursesDB.courses.length) { // nếu chưa chạy hết course list
            if (gCoursesDB.courses[vIndex].id == paramCourseId) { // nếu id của course ở index hiện tại bằng Id của course cần xóa
                gCoursesDB.courses.splice(vIndex, 1); // xóa course ở index là vIndex
                alert("Xóa thành công cho course có Id là " + gCourseId + "!");
                return //ngắt vòng lặp
            }
            else {
                vIndex ++ // nếu course có index hiện tại không đúng id thì tăng index thêm 1
            }
        }
        if (vIndex = gCoursesDB.courses.length) { // nếu index chạy qua course cuối cùng vẫn không đúng id
            alert("Xóa thất bại!")
        }
    }
})